#!/bin/bash

echo "Checking if we have 'dangling' docker images"

if [[ `docker images -f dangling=true -q | wc -l` -gt 0 ]]
then
    echo "Cleaning up 'dangling' docker images"
    docker rmi $(docker images -f dangling=true -q)
else
    echo "Currently no 'dangling' docker images"
fi
