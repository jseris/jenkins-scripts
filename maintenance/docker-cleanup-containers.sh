#!/bin/bash

echo "Checking if we have containers for which status=exited"

if [[ `docker ps -a -q -f status=exited | wc -l` -gt 0 ]]
then
    echo "Cleaning up containers which are in exit status"
    docker rm -v $(docker ps -a -q -f status=exited)
else
    echo "Currently no containers for which status=exited"
fi
