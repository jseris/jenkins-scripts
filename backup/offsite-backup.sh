#!/bin/bash

. /root/.passphrase

export PASSPHRASE

echo "##### DO 1 FULL BACKUP PER MONTH; OTHERS INCREMENTAL #####"
duplicity --full-if-older-than 1M --log-file /var/log/duplicity.log /var/opt/backup file:///var/opt/stack/backups/nasi
duplicity --full-if-older-than 1M --log-file /var/log/duplicity.log /var/docker/volumes/jenkins file:///var/opt/stack/backups/nasi-data-containers/jenkins

echo "##### CLEANING UP BACKUPS OLDER THAN 2 MONTHS ####"
duplicity remove-older-than 2M --log-file /var/log/duplicity.log --force file:///var/opt/stack/backups/nasi
duplicity remove-older-than 2M --log-file /var/log/duplicity.log --force file:///var/opt/stack/backups/nasi-data-containers/jenkins

echo "##### SHOW STATUS OF BACKUPS #####"
duplicity collection-status file:///var/opt/stack/backups/nasi
duplicity collection-status file:///var/opt/stack/backups/nasi-data-containers/jenkins

echo "##### FORCE CLEANUP #####"
duplicity cleanup --force --extra-clean file:///var/opt/stack/backups/nasi
duplicity cleanup --force --extra-clean file:///var/opt/stack/backups/nasi-data-containers/jenkins

unset PASSPHRASE
